const express = require("express");
const passport = require("passport");
const acl = require("../middleware/acl");

const router = express.Router();
//controllers
const { signup, signin, getUsers } = require("../controllers/userControlers");

router.post(
  "/signin",
  passport.authenticate("local", { session: false }),
  signin
);

router.post("/signup", signup);

router.get("/list-users", passport.authenticate("jwt",{session:false}),acl,getUsers);

module.exports = router;
