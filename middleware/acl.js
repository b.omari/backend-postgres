'use strict';

function acl(req, res, next) {

    if (req.user.isAdmin) {
        next()
    } else {
        res.status(401).send("unauthorized")
    }
}

module.exports = acl;
