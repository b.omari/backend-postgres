const express = require('express');
const app = express();
const db = require('./db/models');
const userRoutes = require("./routes/userRoutes");
const passport = require("passport");
const cors = require("cors");
const { localStrategy, jwtStrategy } = require("./middleware/passport");

const PORT = 8080;

app.use(cors());

app.use(express.json());

app.use(passport.initialize());

passport.use(localStrategy);

passport.use(jwtStrategy);


app.use(userRoutes);

db.sequelize.sync();

//error middleware
app.use((err, req, res, next) => {
  res.status(err.status || 500).json({
    message: err.message || "Internal Server Error",
  });
});

app.get('/', (req, res) => {
res.send('Hello World');	
});

const run = async () => {
  try {
    await db.sequelize.authenticate();
    console.log("Connection to the database successful!");
    await app.listen(PORT, () => {
      console.log(`The application is running on localhost: ${PORT}`);
    });
  } catch (error) {
    console.error("Error connecting to the database: ", error);
  }
};
run();


// app.listen(PORT, function () {
//   console.log(`Server listening on port ${PORT}`);
// });
